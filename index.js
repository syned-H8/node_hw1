const express = require('express');
const morgan = require('morgan');
const app = express();

const router = require('./router');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', router);

app.listen(8080, () => {
    console.log('Server works at port 8080!');
});
