const fs = require('fs');
const path = require('path');

const uploadedFilesPath = 'uploaded_files';
const extensions = /^.*\.(log|txt|json|yaml|xml|js)$/;

const passwords = [];

module.exports.createFile = (req, res) => {
    try {
        if (!fs.existsSync(`./${uploadedFilesPath}`)) {
            fs.mkdir(`./${uploadedFilesPath}`, (err) => {
                if (err) {
                    res.status(500).json({ message: 'Server error' });
                };
              });
        }
    } catch(e) {
        console.log("An error occurred.")
    }

    if (!req.body.filename || !req.body.content) {
        res.status(400).json({ message: 'Please specify \'content\' parameter' });
    } else if (!extensions.test(req.body.filename)) {
        res.status(400).json({ message: 'Improper file extension' });
    } else {
        fs.writeFile(`./${uploadedFilesPath}/${req.body.filename}`, req.body.content, err => {
            if (err) {
                res.status(500).json({ message: 'Server error' });
            } else {
                if(req.query.password) {
                    passwords.push({
                        filename: req.body.filename,
                        password: req.query.password
                    });
                }

                res.status(200).json({ message: 'File created successfully' });
            }
        });
    }
}

module.exports.getFiles = (req, res) => {
    fs.readdir(uploadedFilesPath, (err, dir) => {
        const files = [];

        if (err) {
            res.status(500).json({ message: 'Server error' });
        } else if(dir.length === 0) {
            res.status(400).json({ message: 'Client error' });
        } else {
            dir.forEach(file => {
                files.push(path.basename(file));
            });
    
            res.status(200).json({
                message: 'Success',
                files
            });
        }
    });
}

module.exports.getFile = (req, res) => {
    fs.readdir(uploadedFilesPath, (err, dir) => {
        if (err) {
            res.status(500).json({ message: 'Server error' });
        } else if (dir.indexOf(req.params.filename) === -1) {
            res.status(400).json({ message: `No file with \'${req.params.filename}\' filename found` });
        } else {
            if (req.query.password) {
                passwords.forEach(elem => {
                    if (elem.filename === req.params.filename && elem.password === req.query.password) {
                        res.status(200).json({
                            message: 'Success',
                            filename: req.params.filename,
                            content: fs.readFileSync(`${uploadedFilesPath}/${req.params.filename}`, 'utf-8'),
                            extension: path.extname(`${uploadedFilesPath}/${req.params.filename}`).slice(1),
                            uploadedDate: fs.statSync(`${uploadedFilesPath}/${req.params.filename}`).ctime
                        });
                    } else {
                        res.status(400).json({ message: 'Invalid password!' });
                    }
                });
            } else {
                res.status(200).json({
                    message: 'Success',
                    filename: req.params.filename,
                    content: fs.readFileSync(`${uploadedFilesPath}/${req.params.filename}`, 'utf-8'),
                    extension: path.extname(`${uploadedFilesPath}/${req.params.filename}`).slice(1),
                    uploadedDate: fs.statSync(`${uploadedFilesPath}/${req.params.filename}`).ctime
                });
            }
        }
    });
}

module.exports.changeFile = (req, res) => {
    if (!req.body.filename || !req.body.newContent) {
        res.status(400).json({ message: 'Please specify \'content\' parameter' });
    } else {
        fs.readdir(uploadedFilesPath, (err, dir) => {
            if (err) {
                res.status(500).json({ message: 'Server error' });
            } else if (dir.indexOf(req.body.filename) === -1) {
                res.status(400).json({ message: `No file with \'${req.body.filename}\' filename found` });
            } else {
                fs.appendFile(`./${uploadedFilesPath}/${req.body.filename}`, `\n${req.body.newContent}`, err => {
                    if (err) {
                        res.status(500).json({ message: 'Server error' });
                    }
            
                    res.status(200).json({ message: 'File modified successfully' });
                });
            }
        });
    }
}

module.exports.deleteFile = (req, res) => {
    if (!req.body.filename) {
        res.status(400).json({ message: 'Please specify \'filename\' parameter' });
    } else {
        fs.unlink(`${uploadedFilesPath}/${req.body.filename}`, (err, dir) => {
            if (err) {
                res.status(500).json({ message: 'Server error' });
            } else {
                res.status(200).json({ message: 'File deleted successfully' });
            }
        });
    }
}
