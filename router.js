const express = require('express');
const router = express.Router();

const controller = require('./fileController');

router.post('/', (req, res) => {
    controller.createFile(req, res);
});

router.get('/', (req, res) => {
    controller.getFiles(req, res);
});

router.get('/:filename', (req, res) => {
    controller.getFile(req, res);
});

router.put('/', (req, res) => {
    controller.changeFile(req, res);
});

router.delete('/', (req, res) => {
    controller.deleteFile(req, res);
});

module.exports = router;
